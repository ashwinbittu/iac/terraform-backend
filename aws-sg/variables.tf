variable "app_env" {
}

variable "app_name" {
}

variable "app_id" {
}

variable "vpc_id" {
  default = ""
}

variable "aws_region" {
  default = ""
}

variable "name" {
  default = ""
}

variable "description" {
  default = ""
}

variable "create_sg" {
  default = false
}
