terraform {
  backend "http" {}
}

provider "aws" {
  region     = var.aws_region
}

module "sg-alb" {
  source = "./terraform-aws-sg"
  app_env   = var.app_env
  app_name   = var.app_name
  app_id   = var.app_id
  aws_vpc_id = var.vpc_id
  name   = var.name
  description = var.description

  ingress_with_cidr_blocks = [
      {
        rule = "https-443-tcp"
        cidr_blocks = "0.0.0.0/0"
      },
      {
        rule = "http-80-tcp"
        cidr_blocks = "0.0.0.0/0"
      }
  ]

  egress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = "0.0.0.0/0"
    }
  ]

}
