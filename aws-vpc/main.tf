terraform {
  backend "http" {}
}

provider "aws" {
  region     = var.aws_region
}

module "vpc" {

  source = "./terraform-aws-vpc"
  
  no_of_subnets = var.no_of_subnets
  aws_vpc_cidr_block   = var.aws_vpc_cidr_block
  app_env   = var.app_env
  app_name   = var.app_name  
  app_id   = var.app_id  
  aws_vpc_instance_tenancy = var.aws_vpc_instance_tenancy

}
