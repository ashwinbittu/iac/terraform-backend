variable "app_env" {
}

variable "app_name" {
}

variable "app_id" {
}

variable "vpc_id" {
  default = ""
}

variable "aws_region" {
  default = ""
}

variable "aws_vpc_instance_tenancy" {
}

variable "no_of_subnets" {
}

variable "aws_vpc_cidr_block" {
}


